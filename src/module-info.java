import io.scenarium.generator.noise.Plugin;
import io.scenarium.pluginManager.PluginsSupplier;

module io.scenarium.generator.noise {
	uses PluginsSupplier;

	provides PluginsSupplier with Plugin;

	requires transitive io.scenarium.flow;

	exports io.scenarium.generator.noise;
	exports io.scenarium.generator.noise.math;
	exports io.scenarium.generator.noise.operator.dataprocessing;

}
