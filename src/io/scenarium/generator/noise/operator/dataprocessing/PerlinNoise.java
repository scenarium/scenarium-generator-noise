/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.generator.noise.operator.dataprocessing;

import io.beanmanager.editors.PropertyInfo;
import io.beanmanager.editors.container.BeanInfo;
import io.scenarium.flow.EvolvedOperator;
import io.scenarium.generator.noise.math.PerlinNoise1D;

public class PerlinNoise extends EvolvedOperator {
	@PropertyInfo(nullable = false)
	@BeanInfo(alwaysExtend = true, inline = true)
	private PerlinNoise1D perlinNoise = new PerlinNoise1D(1, 1, 2, 6, 0.2, 0);
	private long firstT;

	@Override
	public void birth() {
		this.firstT = -1;
	}

	@Override
	public void death() {}

	public Double process(Double value) {
		long t = getTimeStamp(0);
		if (this.firstT == -1)
			this.firstT = t;
		return value + this.perlinNoise.getValue((t - this.firstT) / 1000.0);
	}

	public PerlinNoise1D getPerlinNoise() {
		return this.perlinNoise;
	}

	public void setPerlinNoise(PerlinNoise1D perlinNoise) {
		this.perlinNoise = perlinNoise;
	}
}
