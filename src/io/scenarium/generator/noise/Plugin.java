package io.scenarium.generator.noise;

import io.beanmanager.PluginsBeanSupplier;
import io.beanmanager.consumer.ClassNameRedirectionConsumer;
import io.scenarium.flow.consumer.OperatorConsumer;
import io.scenarium.flow.consumer.PluginsFlowSupplier;
import io.scenarium.generator.noise.operator.dataprocessing.PerlinNoise;
import io.scenarium.pluginManager.PluginsSupplier;

public class Plugin implements PluginsSupplier, PluginsFlowSupplier, PluginsBeanSupplier {
	@Override
	public void populateOperators(OperatorConsumer operatorConsumer) {
		operatorConsumer.accept(PerlinNoise.class);
	}

	@Override
	public void populateClassNameRedirection(ClassNameRedirectionConsumer classNameRedirectionConsumer) {
		classNameRedirectionConsumer.accept("org.scenarium.operator.dataprocessing.PerlinNoise", "io.scenarium.generator.noise.operator.dataprocessing.PerlinNoise");
	}

}
