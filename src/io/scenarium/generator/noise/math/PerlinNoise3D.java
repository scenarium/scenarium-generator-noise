/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.generator.noise.math;

import io.scenarium.generator.noise.internal.Log;

public class PerlinNoise3D extends PerlinNoise {

	public static void main(String[] args) {
		PerlinNoise3D pn = new PerlinNoise3D(1, 1 / 1000.0, 2, 6, 60 / 1000.0, 0);
		double min = Double.MAX_VALUE;
		double max = -Double.MAX_VALUE;
		int percentInRange = 0;
		for (int i = 0; i < 10000000; i++) {
			double val = pn.getValue(Math.random() * 1000000000, Math.random() * 1000000000, Math.random() * 1000000000);
			if (val < min)
				min = val;
			else if (val > max)
				max = val;
			if (val >= -1 && val <= 1)
				percentInRange++;
			// Log.info(val);
		}
		Log.info(min + " -> " + max + ": " + percentInRange / 10000000.0);
	}

	public PerlinNoise3D() {
		super();
	}

	public PerlinNoise3D(double amplitude, double frequency, double lacunarity, int octaveCount, double persistence, int seed) {
		super(amplitude, frequency, lacunarity, octaveCount, persistence, seed);
	}

	public double getValue(double x, double y, double z) {
		double value = 0;
		double signal = 0;
		double curPersistence = 1;
		double nx, ny, nz;
		int seed;
		x *= this.frequency;
		y *= this.frequency;
		z *= this.frequency;
		for (int curOctave = 0; curOctave < this.octaveCount; curOctave++) {
			// Make sure that these doubleing-point values have the same range as a 32-
			// bit integer so that we can pass them to the coherent-noise functions.
			nx = makeInt32Range(x);
			ny = makeInt32Range(y);
			nz = makeInt32Range(z);
			// Get the coherent-noise value from the input value and add it to the
			// final result.
			seed = this.seed + curOctave & 0xffffffff;
			signal = gradientCoherentNoise3D(nx, ny, nz, seed, this.noiseQuality);
			value += signal * curPersistence;
			// Prepare the next octave.
			x *= this.lacunarity;
			y *= this.lacunarity;
			z *= this.lacunarity;
			curPersistence *= this.persistence;
		}
		return value * this.amplitude;
	}

	private double gradientCoherentNoise3D(double x, double y, double z, int seed, NoiseQuality noiseQuality) {
		// Create a unit-length cube aligned along an integer boundary. This cube
		// surrounds the input point.
		int x0 = x > 0.0 ? (int) x : (int) x - 1;
		int x1 = x0 + 1;
		int y0 = y > 0.0 ? (int) y : (int) y - 1;
		int y1 = y0 + 1;
		int z0 = z > 0.0 ? (int) z : (int) z - 1;
		int z1 = z0 + 1;
		// Map the difference between the coordinates of the input value and the
		// coordinates of the cube's outer-lower-left vertex onto an S-curve.
		double xs = 0, ys = 0, zs = 0;
		switch (noiseQuality) {
		case QUALITY_FAST:
			xs = x - x0;
			ys = y - y0;
			zs = z - z0;
			break;
		case QUALITY_STD:
			xs = sCurve3(x - x0);
			ys = sCurve3(y - y0);
			zs = sCurve3(z - z0);
			break;
		case QUALITY_BEST:
			xs = sCurve5(x - x0);
			ys = sCurve5(y - y0);
			zs = sCurve5(z - z0);
			break;
		default:
			throw new IllegalArgumentException("NoiseQuality: " + noiseQuality + " is not supported");
		}
		// Now calculate the noise values at each vertex of the cube. To generate
		// the coherent-noise value at the input point, interpolate these eight
		// noise values using the S-curve value as the interpolant (trilinear
		// interpolation.)
		double n0, n1, ix0, ix1, iy0, iy1;
		n0 = gradientNoise3D(x, y, z, x0, y0, z0, seed);
		n1 = gradientNoise3D(x, y, z, x1, y0, z0, seed);
		ix0 = linearInterp(n0, n1, xs);
		n0 = gradientNoise3D(x, y, z, x0, y1, z0, seed);
		n1 = gradientNoise3D(x, y, z, x1, y1, z0, seed);
		ix1 = linearInterp(n0, n1, xs);
		iy0 = linearInterp(ix0, ix1, ys);
		n0 = gradientNoise3D(x, y, z, x0, y0, z1, seed);
		n1 = gradientNoise3D(x, y, z, x1, y0, z1, seed);
		ix0 = linearInterp(n0, n1, xs);
		n0 = gradientNoise3D(x, y, z, x0, y1, z1, seed);
		n1 = gradientNoise3D(x, y, z, x1, y1, z1, seed);
		ix1 = linearInterp(n0, n1, xs);
		iy1 = linearInterp(ix0, ix1, ys);

		return linearInterp(iy0, iy1, zs);
	}

	double gradientNoise3D(double fx, double fy, double fz, int ix, int iy, int iz, int seed) {
		// Randomly generate a gradient vector given the integer coordinates of the
		// input value. This implementation generates a random number and uses it
		// as an index into a normalized-vector lookup table.
		int vectorIndex = X_NOISE_GEN * ix + Y_NOISE_GEN * iy + Z_NOISE_GEN * iz + SEED_NOISE_GEN * seed & 0xffffffff;
		vectorIndex ^= vectorIndex >> SHIFT_NOISE_GEN;
		vectorIndex &= 0xff;
		double xvGradient = this.gRandomVectors[vectorIndex << 2];
		double yvGradient = this.gRandomVectors[(vectorIndex << 2) + 1];
		double zvGradient = this.gRandomVectors[(vectorIndex << 2) + 2];
		// Set up us another vector equal to the distance between the two vectors
		// passed to this function.
		double xvPoint = fx - ix;
		double yvPoint = fy - iy;
		double zvPoint = fz - iz;
		// Now compute the dot product of the gradient vector with the distance
		// vector. The resulting value is gradient noise. Apply a scaling value
		// so that this noise value ranges from -1.0 to 1.0.
		return (xvGradient * xvPoint + yvGradient * yvPoint + zvGradient * zvPoint) * 2.12;
	}
}
